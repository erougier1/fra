---
author: Franck Chambon
title: Nombres de chemins dans une grille (2)
tags:
  - maths+
  - boucle
  - a_trou
---
# Énumération des chemins dans une grande grille

Dans une **grande** grille de taille $n×m$, on souhaite compter tous les chemins allant du coin inférieur gauche (au Sud-Ouest) vers le coin supérieur droit (au Nord-Est).

:warning: Dans cet exercice, on pourra avoir $0\leqslant n \leqslant 1000$ et $0\leqslant m \leqslant 1000$.

Les seuls mouvements autorisés sont :

- ↑ Aller au Nord d'une unité.
- → Aller à l'Est d'une unité.

!!! info "Les chemins pour aller de $(0, 0)$ à $(4, 3)$"

    Il y en a 35.

    ![](images/chemin_0.svg)
    ![](images/chemin_1.svg)
    ![](images/chemin_2.svg)
    ![](images/chemin_3.svg)
    ![](images/chemin_4.svg)
    ![](images/chemin_5.svg)
    ![](images/chemin_6.svg)
    ![](images/chemin_7.svg)
    ![](images/chemin_8.svg)
    ![](images/chemin_9.svg)
    ![](images/chemin_10.svg)
    ![](images/chemin_11.svg)
    ![](images/chemin_12.svg)
    ![](images/chemin_13.svg)
    ![](images/chemin_14.svg)
    ![](images/chemin_15.svg)
    ![](images/chemin_16.svg)
    ![](images/chemin_17.svg)
    ![](images/chemin_18.svg)
    ![](images/chemin_19.svg)
    ![](images/chemin_20.svg)
    ![](images/chemin_21.svg)
    ![](images/chemin_22.svg)
    ![](images/chemin_23.svg)
    ![](images/chemin_24.svg)
    ![](images/chemin_25.svg)
    ![](images/chemin_26.svg)
    ![](images/chemin_27.svg)
    ![](images/chemin_28.svg)
    ![](images/chemin_29.svg)
    ![](images/chemin_30.svg)
    ![](images/chemin_31.svg)
    ![](images/chemin_32.svg)
    ![](images/chemin_33.svg)
    ![](images/chemin_34.svg)

Écrire une fonction telle que `nb_chemins(n, m)` renvoie le nombre de chemins allant de $(0, 0)$ jusqu'à $(n, m)$.

Pour ce faire, on admettra que `nb_chemins(n, m)` est égal à $\binom{n}{n+m}$.

$$\binom{n}{n+m} = \dfrac{(n+m)!}{n!m!}$$

Où :

- $n! = 1×2×3×\cdots×(n-1)×n$
- $m! = 1×2×3×\cdots×(m-1)×m$
- $(n+m)! = 1×2×3×\cdots×(n+m-1)×(n+m)$

!!! tip "Utilisation pour `nb_chemins(4, 3)`"

    - $4! = 1×2×3×4$
    - $3! = 1×2×3$
    - $(4+3)! = 1×2×3×4×5×6×7$

    $$\binom{4}{4+3} = \dfrac{1×2×3×4×5×6×7}{1×2×3×4~×~1×2×3}$$
    
    $$\binom{4}{4+3} = \dfrac{5×6×7}{1×2×3} = 35$$

!!! warning "Utilisation pour `nb_chemins(n, m)`"

    - $n! = 1×2×3×\cdots×n$
    - $m! = 1×2×3×\cdots×m$
    - $(n+m)! = 1×2×3×\cdots×(n+m) = 1×2×3×\cdots×n×(n+1)×\cdots×(n+m)$

    $$\binom{n}{n+m} = \dfrac{1×2×3×\cdots×(n+m)}{1×2×3×\cdots×n~×~1×2×3\cdots×m}$$
    
    $$\binom{n}{n+m} = \dfrac{\xcancel{(1×2×3×\cdots×n)}~×~(n+1)×\cdots×(n+m)}{\xcancel{1×2×3×\cdots×n}~×~1×2×3×\cdots×m}$$

!!! done "En conclusion"

    On déduit la formule simple, à utiliser ici :

    `nb_chemins(n, m)` est égal à $\dfrac{(n+1)×(n+2)×(n+3)×\cdots×(n+m)}{1×2×3×\cdots×m}$


- Pour calculer `nb_chemins(n, m)`,
    - on fera une boucle pour calculer le numérateur
    - et une autre boucle pour calculer le dénominateur
    - on terminera par la division entière.
- On complètera le code :

{{ py_sujet('exo') }}

!!! example "Exemples"

    ```pycon
    >>> nb_chemins(3, 3)
    20
    >>> nb_chemins(4, 2)
    15
    >>> nb_chemins(4, 3)
    35
    ```

Contraintes : Ici, $0\leqslant n \leqslant 1000$ et $0\leqslant m \leqslant 1000$.

{{ IDE('exo') }}
